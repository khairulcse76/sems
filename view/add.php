
<?php 

include_once './inc/header.php';
?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>
                        <a class="btn btn-success" href="index.php">Back to List</a>
                        <a class="btn btn-info pull-right" href="">View All</a>
                    </h2>
                </div>
                <div class="panel-body">
                    <form action="addrequest.php" method="POST">
                        <div class="form-group">
                            <label for="name">Student Name</label>
                            <input type="text" name="st_name" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="roll">Roll</label>
                            <input type="number" name="st_roll" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <input class="btn btn-success" type="submit" name="submit" value="Add/Submit"/>
                        </div>
                    </form>
                </div>
            </div>
<?php include_once './inc/footer.php'; ?>
            