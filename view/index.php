<?php 
include_once '../vendor/autoload.php';
use sems\Student\student;

$student=new student();
$ViewData=$student->viewAttendance();
$date= date('d M Y');


?>

<?php include_once './inc/header.php'; ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>
                        <a class="btn btn-success" href="add.php">Add Student</a>
                        <a class="btn btn-info pull-right" href="">View All</a>
                    </h2>
                </div>
                <div class="panel-body">
                    <div class="well text-center text-info" style="font-size: 24px;">
                        <span class="text-center"><strong>Date:-</strong> <?php echo $date; ?></span>
                    </div>
                    <form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="25%">SL</th>
                                    <th width="25%">Name</th>
                                    <th width="25%">roll</th>
                                    <th width="25%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php 
                                $id=1;
                                   foreach ($ViewData as $value){
                                    
                                  ?>
                                <tr>
                                    <td><?php echo $id++ ?></td>
                                    <td><?php echo $value['st_name'] ?></td>
                                   <td><?php echo $value['st_roll'] ?></td>
                                    <td>
                                        <label><input type="radio" name="attent" value="present" checked> present<br></label>
                                        <label><input type="radio" name="attent" value="absent" > Absent<br></label>
                                    </td>
                                </tr>
                                        
                                       <?php   } ?>
                                <tr>
                                    <td colspan="4" class="text-right">
                                        <input class="btn btn-success" type="submit" name="submit" value="Submit"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
<?php include_once './inc/footer.php'; ?>
            